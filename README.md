# HOW TO SET UP PACKAGE
1. Move into *catkin_ws/src*
2. Clone the git repository into a folder called "project"
    *git clone git@gitlab.com:comp3631/Group7.git project*
3. You may have to create a new package called project
    *catkin_create_pkg project*
4. Move into the new "project" folder
5. Checkout the "project" branch
    *git checkout project*
6. Should now have src/ and launch/ folders
7. Verify package works by running catkin_make (in ubuntu!)

DEMO
1. roslaunch turtlebot_bringup minimal.launch
2. roslaunch turtlebot_navigation amcl_demo.launch map_file:=/home/turtlebot/catkin_ws/src/project/launch/map.yaml
3. roslaunch turtlebot_rviz_launchers view_navigation.launch
4. roslaunch ar_track_alvar pr2_indiv.launch
5. rosrun project image_finder.py
6. rosrun project rotate.py
7. rosrun project ar_listener.py
8. clear.sh
9. Estimate position using rviz
10. rosrun project navigation.py

Attribution
---

Repository icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
