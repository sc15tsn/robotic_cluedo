#!/usr/bin/env python

'''
Feature homography
==================

Example of using features2d framework for video homography matching.
ORB features and FLANN matcher are used. The actual tracking is implemented by
PlaneTracker class in plane_tracker.py

Inspired by http://www.youtube.com/watch?v=-ZNYoL8rzPY

video: http://www.youtube.com/watch?v=FirtmYcC0Vc

Usage
-----
feature_homography_from_file1.py [<video source>]

'''

# Python 2/3 compatibility
from __future__ import print_function

import numpy as np
import cv2
import os

# local modules
import common
from common import getsize, draw_keypoints
from plane_tracker import PlaneTracker


class ImageProcessor:
    def __init__(self):

        self.frame = None
        self.tracker = PlaneTracker()

		#Add template images to tracker
        imageFiles = ["mustard.png", "peacock.png", "plum.png", "revolver.png", "rope.png", "scarlet.png", "wrench.png"]
        directory = os.path.expanduser('~/catkin_ws/src/project/RefImages/')
        for image in imageFiles:
            filename = directory + image
            print("Reading: " + filename)
            trackImage = cv2.imread(filename)
            if (trackImage == None):
                print("Could not load image: " + str(filename))
                return

            rect = (0,0,trackImage.shape[1],trackImage.shape[0])
            self.tracker.add_target(trackImage.copy(), rect, image)


    def log(self, message):
        with open("log.txt", "a") as f:
            f.write(message + '\n')




    def processImage(self, filepath):

        self.frame = cv2.imread(filepath)

        if type(self.frame) == "None":
            print("Could not load image.")
            return

        #get height and width of image
        w, h = getsize(self.frame)
        #create a blank image
        vis = np.zeros((h, w, 3), np.uint8)

        #load the image into the blank frame
        vis[:h,:w] = self.frame

        tracked = self.tracker.track(self.frame)

        if len(tracked) > 0:

                for tracked_ob in tracked:

                    print ('Found ' + tracked_ob.target.data)
                    self.log("Found " + tracked_ob.target.data)
                    # Calculate Homography
                    h, status = cv2.findHomography(tracked_ob.p0, tracked_ob.p1)
                    return (True, tracked_ob.target.data)
        else:
            print("No object found!")
            # self.log("No object found")
            return (False, None)




"""
if __name__ == '__main__':
    print(__doc__)

    import sys
    try:
        video_src = sys.argv[1]
    except:
        video_src = 0
    App(video_src).run()
"""
