#!/usr/bin/env python
import roslib
import rospy
import tf
import time
import cv2
from sensor_msgs.msg import Image
from std_msgs.msg import Bool

arMarkerFound = False
numberOfMarkers = 0

def callback(data):
    global arMarkerFound

if __name__ == '__main__':
    rospy.init_node('ar_listener',anonymous=True)

    rospy.Subscriber("camera/rgb/image_raw", Image, callback)

    listener = tf.TransformListener() # Create a tf listener
    publisher = rospy.Publisher("arMarkerFound", Bool, queue_size=10)

    tfbroadcast = tf.TransformBroadcaster()
    previousMarker = 0
    while not rospy.is_shutdown():

        time.sleep(1.0) # Sleeping for 3 second to listen to tf messages.
        #initialise the translation and rotation to -1, -1
        (trans, rot) = (-1,-1)
        try:
            #get tf to find the positon of the ar marker, relative to the map
            (trans,rot) = listener.lookupTransform('/map', '/ar_marker_0', rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            pass

        #if the trans and rot are still -1, then the ar marker was not found
        if (trans == -1 and rot == -1):
            print("No ar marker found.")
            arMarkerFound = False
            publisher.publish(False)
        else:

            # Check if we have already considered this marker
            if previousMarker == 0:
            	print("Find new Marker")
                previousMarker = trans
                publisher.publish(True)

            elif (trans[0] < previousMarker[0] - 0.7 and previousMarker[0] + 0.7 > trans[0]) or (trans[1] < previousMarker[1] - 0.7 and previousMarker[1] + 0.7 > trans[1]):
                print("Find different Marker", trans, previousMarker)
                previousMarker  = trans
                arMarkerFound = True
                publisher.publish(True)
            else:
            	print("Same marker")
            # print "Translation:"
            # print trans
            # print "\n"
            # print "Rotation"
            # print rot
            # print "\n"

            #broadcast a frame that is 0.5 away from the ar marker. This is where the robot should move to.
            tfbroadcast.sendTransform((0.0, 0.0, 0.5),(0.0, 0.0, 0.0, 1.0),rospy.Time.now(),"ar_view_pose","ar_marker_0")
