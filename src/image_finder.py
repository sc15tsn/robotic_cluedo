#!/usr/bin/env python

import rospy
import cv2
import time
import os
import rospkg
from sensor_msgs.msg import Image
from std_msgs.msg import Bool
from cv_bridge import CvBridge, CvBridgeError
from ImageProcessor import *

#Rospy node that finds a contour corresponding to the image, draws a bounding box around the image, and takes a picture

#Subscribe: Message to indicate robot is positioned near AR marker

lookForImage = False
#screenshotPath = "~/modules/year3/robotics/catkin_ws/src/project/detections/"
detections = 0
pictureArea = 200000
prevLookForImage = False

#Finds the contour in an image with a size closest to the given area
def findPictureContour(contours, area):

	#If there are no contours, return
	if not contours:
		return

	#Otherwise loop through the contours, finding the one with the smallest size difference from area
	pictureContour = contours[0]

	for contour in contours:

		pictureContourAreaError = abs(cv2.contourArea(pictureContour) - area)
		currentContourAreaError = abs(cv2.contourArea(contour) - area)

		if currentContourAreaError < pictureContourAreaError:

			pictureContour = contour

	return pictureContour


#Called when a detection message is recieved from the navigation node
def readyCallback(data):

	global lookForImage

	#Save whether or not the robot should look for an image as a global variable
	if data.data:
		lookForImage = True
	else:
		lookForImage = False


#Called when an image message is recieved from the camera
def detectImage(data):

	global detections
	global lookForImage
	global prevLookForImage

	#If the robot should look for an image, do so
	if lookForImage:
		findImage(data)

	#If the robot has finished looking for an image, increase the detections
	#if(prevLookForImage == True and lookForImage == False):
	#	detections += 1

	#Save the boolean for the next iteration
	#prevLookForImage = lookForImage


#Tries to find a character in the camera feed
def findImage(data):

	global prevDetectionMessage
	global detections

	print("CALLBACK!")
	bridge = CvBridge()

	#Convert image to format opencv can use
	cvImg = bridge.imgmsg_to_cv2(data, 'bgr8')

	#Find contours in image
	smoothImg = cv2.GaussianBlur(cvImg, (5, 5), 1)
	edges = cv2.Canny(smoothImg, 100, 200)
	contours, h1 = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

	#Find contour corresponding to picture in image
	pictureContour = findPictureContour(contours, pictureArea)

	#Get bounding box of contour
	pictureRect = cv2.boundingRect(pictureContour)

	#Draw it
	cv2.rectangle(cvImg, (pictureRect[0], pictureRect[1]), (pictureRect[0] + pictureRect[2], pictureRect[1] + pictureRect[3]), (255, 0, 255), 1)

	#Save image to a temporary file to perform image processing
	rospackage = rospkg.RosPack()
	pathName = os.path.abspath(rospackage.get_path('project') + "/detections/temp.jpeg")
	cv2.imwrite(pathName, cvImg)


	#Notify other nodes that this one is finished
	pub.publish(Bool(True))

	#Draw window
	#cv2.namedWindow('Image_Feed')
	#cv2.imshow('Image_Feed', cvImg)

	#Wait a moment
	#cv2.waitKey(20)

	validDetections = []

	#Process image to see if there is a character present
	processResult = processor.processImage(pathName)

	#If there is, save the image, if not, ignore
	if(processResult[0]):
		validDetections.append(processResult[1])
		finalPathName = os.path.abspath(rospackage.get_path('project') + "/detections/image" + str(detections) + ".jpeg")
		cv2.imwrite(finalPathName, cvImg)
		detections += 1

	os.remove(pathName);

if __name__ == "__main__":

	#Create new ros node
	rospy.init_node('image_finder', anonymous=True)

	#Create an image processor object
	processor = ImageProcessor()

	#Set up publishing to finished topic
	pub = rospy.Publisher('image_found', Bool, queue_size=0)

	#Subscribe to the detection topic
	rospy.Subscriber("readyToDetect", Bool, readyCallback)

	#Subscribe to image topic
	rospy.Subscriber("camera/rgb/image_raw", Image, detectImage)

	#Loop until shutdown
	rospy.spin()

	#Destroy image window
	#cv2.destroyWindow('Camera_Feed')
