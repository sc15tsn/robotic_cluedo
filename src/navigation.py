#!/usr/bin/env python

'''
Copyright (c) 2015, Mark Silliman
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

# TurtleBot must have minimal.launch & amcl_demo.launch
# running prior to starting this script
# For simulation: launch gazebo world & amcl_demo prior to run this script

import rospy
import roslib
import tf
import time
import numpy as np
import os
import rospkg
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, Point, Quaternion
from nav_msgs.msg import MapMetaData, OccupancyGrid
from std_msgs.msg import Bool
from math import sqrt
import cv2

class GoToPose():
    def __init__(self):

        self.goal_sent = False
        self.TOLERANCE = 0.5
        self.previousMarkers = []

        # What to do if shut down (e.g. Ctrl-C or failure)
        rospy.on_shutdown(self.shutdown)

        # Tell the action client that we want to spin a thread by default
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        rospy.loginfo("Wait for the action server to come up")

        # Allow up to 5 seconds for the action server to come up
        self.move_base.wait_for_server(rospy.Duration(5))

        #create a pub that will let code know when it can search for an ar marker
        self.searchPub = rospy.Publisher("readyToSearch", Bool, queue_size=10)
        #create a publisher that will let other code know when we have found an ar marker
        self.detectPub = rospy.Publisher("readyToDetect", Bool, queue_size=10)

        self.rotatePub = rospy.Publisher("readToRotate", Bool, queue_size=10)

    def goto(self, pos, quat):

        # Send a goal
        self.goal_sent = True
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose = Pose(Point(pos['x'], pos['y'], 0.000),
                                     Quaternion(quat['r1'], quat['r2'], quat['r3'], quat['r4']))

        # Start moving
        self.move_base.send_goal(goal)

        # Allow TurtleBot up to 60 seconds to complete task
        success = self.move_base.wait_for_result(rospy.Duration(60))

        state = self.move_base.get_state()
        result = False

        if success and state == GoalStatus.SUCCEEDED:
            # We made it!
            result = True
        else:
            self.move_base.cancel_goal()

        self.goal_sent = False
        return result

    def shutdown(self):
        if self.goal_sent:
            self.move_base.cancel_goal()
        rospy.loginfo("Stop")
        rospy.sleep(1)

    def GoToPoint(self, givenX, givenY, givenTheta):


        try:


            # Customize the following values so they are appropriate for your location
            x = givenX
            y = givenY
            theta = givenTheta
            position = {'x': x, 'y' : y}
            quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : np.sin(theta/2.0), 'r4' : np.cos(theta/2.0)}

            rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
            success = navigator.goto(position, quaternion)

            if success:
                rospy.loginfo("Hooray, reached the desired pose")
            else:
                rospy.loginfo("The base failed to reach the desired pose")

            # Sleep to give the last log messages time to be sent
            rospy.sleep(1)

        except rospy.ROSInterruptException:
            rospy.loginfo("Ctrl-C caught. Quitting")

    def saveCoord(self, coords):
        rospackage = rospkg.RosPack()
        filepath = os.path.abspath(rospackage.get_path('project') + "/detectionLocations.txt")
        with open(filepath, 'a' ) as f:
            f.write("Detection at: " + str(coords[0]) + ", " + str(coords[1]) + "\n")

    def checkForPreviousMarkers(self, x, y):
        for prev in self.previousMarkers:
            if prev[0] - 0.5 <= x and x <= prev[0] + 0.5 and prev[1] - 0.5 <= y and y <= prev[1] + 0.5:
                print("Done this marker already!")
                return False

        return True


# Get the map metadata
def getMapData(data):
    global map_data
    map_data = data

# Get the occupancy grid
def getOccupancyGrid(data):
    global map_occupancy
    map_occupancy = data.data

# Called when AR marker is found
def arFound(data):
    # global navigator
    # global stopMoving
    # stopMoving = data.data
    # if stopMoving == True:
    #     #cancel movement
    #     navigator.move_base.cancel_all_goals()
    #     print("Stopping during movement")
    pass

# Called when the robot needs to search
def startSearch(data):
    global map_data, map_occupancy
    global occupancyGrid
    global navigator
    global stopMoving
    global cx, cy
    global x1, x2, y1, y2
    global continueX

    if data.data == True and stopMoving == False:

        # Get the coordinates
        for p1 in range(continueX, len(cx)):
            continueX += 1

            for p2 in range(len(cy)):
                # Check if we need to stop
                if stopMoving == True:
                    print("Stopping!")
                    return

                point = [cx[p1], cy[p2]]

                # Check the occupancy
                a = ((point[0] - x1) * map_data.width) / (x2 - x1)
                b = ((point[1] + y1) * map_data.height) / (y1 - y2)

                if a < len(occupancyGrid[0]) and b < len(occupancyGrid):
                    # print(b, a)

                    if occupancyGrid[int(b)][int(a)] == 0:

                        # Go to the position
                        position = {'x': point[0], 'y' : point[1]}
                        theta = 0
                        quaternion = {'r1' : 0.000, 'r2' : 0.000, 'r3' : np.sin(theta/2.0), 'r4' : np.cos(theta/2.0)}
                        rospy.loginfo("Go to (%s, %s) pose", position['x'], position['y'])
                        navigator.goto(position, quaternion)




def loadMapData():

    try:

        global map_data, map_occupancy
        global occupancyGrid
        global navigator
        global cx, cy
        global x1, x2, y1, y2

        searchSensitivity = 70

        # Subscribe to the topics
        rospy.Subscriber('/map_metadata', MapMetaData, getMapData)
        rospy.Subscriber('/map', OccupancyGrid, getOccupancyGrid)
        rospy.Subscriber('/readyToSearch', Bool, startSearch)
        rospy.Subscriber('/arMarkerFound', Bool, arFound)
        rospy.sleep(2)

        # Reshape occupancy grid
        rawOccupancyGrid = np.asarray(map_occupancy)
        rawOccupancyGrid = rawOccupancyGrid.reshape(map_data.height, map_data.width)

        # Get the coordinates
        x1 = map_data.origin.position.x
        y1 = -map_data.origin.position.y
        x2 = x1 + map_data.resolution * map_data.width
        y2 = y1 - map_data.resolution * map_data.height
        topLeftCorner = [x1, y1]
        bottomRightCorner = [x2, y2]

        # Increase size of walls
        occupancyGrid = rawOccupancyGrid.copy()

        length = 5
        for i in range(len(rawOccupancyGrid)):
            for j in range(len(rawOccupancyGrid[0])):
                if rawOccupancyGrid[i][j] > 0:

                    # Left
                    for l in range(length):
                        try:
                            occupancyGrid[i-l][j] = 100
                        except Exception, e:
                            pass

                    # Right
                    for l in range(length):
                        try:
                            occupancyGrid[i+l][j] = 100
                        except Exception, e:
                            pass

                    # Up
                    for l in range(length):
                        try:
                            occupancyGrid[i][j+l] = 100
                        except Exception, e:
                            pass

                    # Down
                    for l in range(length):
                        try:
                            occupancyGrid[i][j-l] = 100
                        except Exception, e:
                            pass



        print(topLeftCorner, bottomRightCorner)

        # Sample points
        cx = np.linspace(topLeftCorner[0], bottomRightCorner[0], searchSensitivity)
        cy = np.linspace(topLeftCorner[1], bottomRightCorner[1], searchSensitivity)

        # rospy.spin()


    except rospy.ROSInterruptException:
        rospy.loginfo("Ctrl-C caught. Quitting")


if __name__ == "__main__":

    #first make sure everyone knows we are not ready to search yet
    #set up the navigator
    global navigator
    global continueX, stopMoving

    continueX = 0
    stopMoving = False

    rospy.init_node('nav_test', anonymous=True)
    navigator = GoToPose()
    navigator.searchPub.publish(False)

    loadMapData()


    #get co-ordinates
    print("Enter x,y co-ordinates of centre of room")
    coords_str = raw_input()

    coords = coords_str.split()

    x = float(coords[0])
    y = float(coords[1])



    listener = tf.TransformListener()



    if (x != -1.0):
        navigator.GoToPoint(x, y, 0)
    else:
        print("Staying here!")




    #now we're at the centre, look for an ar marker

    navigator.searchPub.publish(True)
    sendMessage = True
    #now we've found an ar marker, move to the correct point to view it
    while not rospy.is_shutdown():


        if len(navigator.previousMarkers) == 2:
            print("DONE")
            navigator.saveCoord(navigator.previousMarkers[1])
            break

        elif len(navigator.previousMarkers) == 1:
            navigator.saveCoord(navigator.previousMarkers[0])

        time.sleep(1.0)
        #initialise the translation and rotation to -1, -1
        (trans, rot) = (-1,-1)
        distance = 0
        try:
            #wait 5 seconds for the tf frames to be published (ar_view_pose takes a while to calculate)
            #listener.waitForTransform('/map', '/ar_view_pose', rospy.Time(0), rospy.Duration(5.0))
            #get tf to find the positon of the ar marker, relative to the map
            (trans, rot) = listener.lookupTransform('/map', '/ar_view_pose', rospy.Time(0))

            #find our current position
            (curPos, curRot) = listener.lookupTransform('/map', '/base_link', rospy.Time(0))
            #calculate the distance between us and the ar_view_pose
            distance = (abs(trans[0] - curPos[0])**2) + (abs(trans[1] - curPos[1])**2)
            distance = sqrt(distance)
            print("Distance to view pose:" + str(distance))

        except:
            #if it doesn't find it, we probably can't see an ar marker now
            pass


        if (trans == -1 and rot == -1):
            print("No ar marker found.")
            #we are not ready to detect an imnage
            navigator.detectPub.publish(False)
            #make sure we are allowed to send a message when we find an ar marker
            sendMessage = True
            # navigator.searchPub.publish(True)
            stopMoving = False
        else:
            #check if we've looked at this ar marker before
            if (navigator.checkForPreviousMarkers(trans[0], trans[1])):

                navigator.previousMarkers.append((trans[0], trans[1]))
                navigator.move_base.cancel_all_goals()
                print("Stopping during movement")
                stopMoving = True
                navigator.GoToPoint(trans[0], trans[1], 0)
                #we can see an ar marker
                navigator.searchPub.publish(False)
                #navigator.saveCoord(trans[0], trans[1])

                print("Arrived at view pose!")
                navigator.rotatePub.publish(True)
                #navigator.detectPub.publish(True)
                #wait 5 secs to allow image finder to work
                time.sleep(60.0)
                navigator.searchPub.publish(True)
                stopMoving = False
                #now we've sent a message don't send another
                sendMessage = False
