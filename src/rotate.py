#!/usr/bin/env python

import rospy
import roslib
import time
from std_msgs.msg import Bool
from geometry_msgs.msg import Twist
arFound = False
pub1 = None
pub2 = None

def rotateCallback(data):
    if data.data:
        rotate()

def arFoundCallback(data):
    global arFound
    arFound = data.data


def rotate():
    global arFound
    global pub1
    global pub2
    print("Rotating!")
    timePassed = 0
    rate = rospy.Rate(10)

    while timePassed < 600:
        myMsg = Twist()
        myMsg.angular.z = 0.2
        pub1.publish(myMsg)
        pub2.publish(True)
        rate.sleep()
        timePassed += 1
        #print(timePassed)

    myMsg = Twist()
    myMsg.angular.z = 0.0
    pub1.publish(myMsg)
    pub2.publish(False)

if __name__ == "__main__":
	rospy.init_node('rotate', anonymous=True)
	pub1 = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=10)
	pub2 = rospy.Publisher("/readyToDetect", Bool, queue_size=10)
	rospy.Subscriber("/readToRotate", Bool, rotateCallback)
	rospy.Subscriber("arMarkerFound", Bool, arFoundCallback)

	#rotate()

	while not rospy.is_shutdown():
	    rospy.spin()
